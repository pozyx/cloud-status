# Cloud Status

Add a message to the cloud web application. Edit status.json to achieve this.

Example:

    {
      "type": "warning",
      "title": "Test 1, 2, 3",
      "description": "This is a warning message"
    }

Available types: `success`, `info`, `warning`, `error`
